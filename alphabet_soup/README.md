# Requirements
Python 3.11

# Usage
Execute "python3 ./alphabet_soup.py <input_file>" to run script on a given input_file.

Execute "python3 ./alphabet_soup.py --help" or "./alphabet_soup.py" with no arguments to bring up 
usage message.

Execute "python3 -m unittest tests/test.py" to run unit tests.
