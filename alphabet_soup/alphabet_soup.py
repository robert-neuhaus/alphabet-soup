#!/usr/bin/env python3.11

import argparse
from collections import defaultdict, namedtuple
import logging
from pathlib import Path
import sys
from typing import Tuple, List, Optional

Grid = namedtuple('Grid', 'rows height width char_locs')
Solution = namedtuple('Solution', 'string start_loc end_loc')


def main(in_path: Path, out_path: Optional[Path] = None, log_level: Optional[int] = None) -> None:
    """ Starting point of script.

    Calls parse_input(), passing path, to parse input for grid layout
    and answers. Calls solve(), passing in grid and answers, to find
    solutions. Prints any found solutions.
    
    Args:
        in_path: Path to file containing grid and answers
        out_path: Path to write solutions to. If not provided, will
            output to STDOUT.
        log_level: The level at which to log. Choices are:
            CRITICAL, FATAL, ERROR, WARN, WARNING, INFO, DEBUG, NOTSET
    """
    if log_level is not None:
        logging.basicConfig(level=log_level)
    try:
        grid, answers = parse_input(in_path)
    except FileNotFoundError:
        sys.exit(f'Error: Unable to locate file "{in_path}"')
    solutions = solve(grid, answers)
    out_lines = []
    for solution in solutions:
        out_lines.append(f'{solution.string} {solution.start_loc[0]}:{solution.start_loc[1]}' 
                         f' {solution.end_loc[0]}:{solution.end_loc[1]}')
    out = '\n'.join(out_lines)
    if out_path is None:
        print(out)
    else:
        with out_path.open('w+') as f:
            f.write(out)


def parse_input(path: Path) -> Tuple[Grid, List[str]]:
    """ Parses file at input path to determine grid layout and answers.
    
    Args:
        path: File to parse for grid layout and answers.
    Returns:
        Tuple containing a Grid namedtuple and a list of answers.
    """
    with path.open('r') as input_file:
    # parse input file for grid and answers
        grid_size = input_file.readline()
        width, height = [int(i) for i in grid_size.split('x')]
        rows = []
        char_locs = defaultdict(list) # store locations of each char
        logging.debug(f'width: {width}\theight: {height}')
        h = 0
        while h < height:
            rows.append([])
            line = input_file.readline().strip().replace(' ', '')
            w = 0
            logging.debug(f' line {h}: {line}')
            for letter in line:
                logging.debug(f'\tletter {w}: {letter}')
                rows[h].append(letter)
                char_locs[letter].append((h, w))
                w += 1
            h += 1
        logging.debug(rows)
        logging.debug(char_locs)
        answers = []
        for line in input_file:
            answers.append(line.strip().replace(' ', ''))
        logging.debug(f'{len(answers)} {answers}')
    return Grid(rows, height, width, char_locs), answers


def solve(grid: Grid, answers: List[str]) -> List[Solution]:
    """ Attempts to locate all answers within the grid.

    Calls search() for each answer with different arg combinations for 
    col_direction and row_direction until a Solution is returned or
    direction combinations are exhausted. 
    
    Args:
        grid: Grid namedtuple to search within for answers.
        answers: List of strings containing answers to search for.
    Returns:
        List of Solution namedtuples.
    """
    solutions = []
    for answer in answers:
        if answer[0] not in grid.char_locs:
        # First char of answer not found in grid
            continue
        for start_loc in grid.char_locs[answer[0]]:
        # Search all directions of all known start locations of first
        # char in an answer until full answer is found
            logging.debug(f'{answer[0]}: {start_loc}')
            solution = (
                search(grid, answer, start_loc, 0, -1) # north
                or search(grid, answer, start_loc, 0, 1) # south
                or search(grid, answer, start_loc, 1, 0) # east
                or search(grid, answer, start_loc, -1, 0) # west
                or search(grid, answer, start_loc, 1, -1) # northeast
                or search(grid, answer, start_loc, 1, 1) # southeast
                or search(grid, answer, start_loc, -1, -1) # northwest
                or search(grid, answer, start_loc, -1, 1) # southwest
            ) 
            if solution:
                solutions.append(solution)
                break
        if not solution:
            logging.warning(f'Unable to find answer "{answer}"')
        else:
            logging.info(f'Found answer {solution}')
    return solutions


def search(grid, answer, start_loc, col_direction, row_direction) -> Optional[Solution]:
    """ Search for given answer within grid.

    Start search at start_loc and walk grid in col_direction and
    row_direction until match found or search extends beyond grid
    boundries.

    Args:
        grid: Grid to search within for answer.
        answer: String to search within grid for.
        start_loc: Starting location to begind search.
        col_direction: Column direction to move within grid.
        row_direction: Row direction to move within grid.
    Returns:
        Solution namedtuple if a solution was found; otherwise, None.
    """
    row_idx, col_idx = start_loc
    logging.debug(f'Search starting at {start_loc}\tcol direction: {col_direction}'
                  f' \trow direction: {row_direction}')
    try:
        for str_idx, answer_char in enumerate(answer[1:]):
        # Walk grid in col_direction and row_direction until match found
            col_idx += col_direction
            row_idx += row_direction
            str_idx += 1
            grid_char = grid.rows[row_idx][col_idx]
            if grid_char != answer_char:
            # Chars don't match; stop search
                logging.debug(f'Grid char {grid_char} at {row_idx}:{col_idx}'
                              f' does not match answer char {answer_char} at {str_idx }'
                              f' in answer {answer}')
                return None
        return Solution(answer, start_loc, (row_idx, col_idx))
    except IndexError:
    # Search extended beyond grid boundries; stop search
        logging.debug(f'Search at {col_idx}:{row_idx} extends beyond grid boundries:' 
                      f' {grid.width}:{grid.height}')
        return None


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='AlphabetSoup')
    parser.add_argument('input_path', metavar='input-path')
    parser.add_argument('-l', '--log', choices=logging.getLevelNamesMapping().keys())
    args = parser.parse_args()
    path = Path(args.input_path)
    main(path, log_level=args.log)
