#!/usr/bin/env python3.11

import filecmp
from pathlib import Path
import unittest

import alphabet_soup

class Test(unittest.TestCase):
    
    def setUp(self):
        """Set attributes used in test cases."""
        self.input_dir_path = Path(__file__).parent.joinpath('input')
        self.output_dir_path = Path(__file__).parent.joinpath('output')
        self.temp_path = Path(__file__).parent.joinpath('temp')

    def compare_files(self, input_path, output_path):
        """Compare output file to a golden file."""
        alphabet_soup.main(input_path, out_path=self.temp_path)
        is_equal = filecmp.cmp(output_path, self.temp_path)
        self.temp_path.unlink()
        return is_equal

    def test0(self):
        """Compare case0 to a golden file."""
        input_path = self.input_dir_path.joinpath('case0.txt')
        output_path = self.output_dir_path.joinpath('case0.txt')
        assert self.compare_files(input_path, output_path)

    def test1(self):
        """Compare case1 to a golden file."""
        input_path = self.input_dir_path.joinpath('case1.txt')
        output_path = self.output_dir_path.joinpath('case1.txt')
        assert self.compare_files(input_path, output_path)

    def test2(self):
        """Compare case2 to a golden file."""
        input_path = self.input_dir_path.joinpath('case2.txt')
        output_path = self.output_dir_path.joinpath('case2.txt')
        assert self.compare_files(input_path, output_path)

    def test_bad_input_file(self):
        """Assert proper Exception and error for nonexistent file."""
        with self.assertRaises(SystemExit) as context:
            input_path = self.input_dir_path.joinpath('bad.txt')
            alphabet_soup.main(input_path)
        assert f'Error: Unable to locate file "{input_path}"' in str(context.exception)

if __name__ == '__main__':
    unittest.main()
